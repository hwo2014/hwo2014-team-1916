var net = require("net");
var JSONStream = require('JSONStream');
var _ = require("underscore");

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
    
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
    
    /*
    return send({
        "msgType": "createRace", "data": {
            "botId": {
                "name": botName,
                "key": botKey
            },
            "trackName": "usa",
            "password": "hopotus",
            "carCount": 2
        }
    });
   */ 
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

var lastPieceIndex = -1;
var lastDistance = -1;
var latestData;
var crashData = [];
var crashDataIndex = 0;
var lastSpeed = 0;
var lastAngle = 0;
var lap = 0;

var trackPieces;
var latestMaxSpeed = 0;

jsonStream.on('data', function (data) {

    if (data.msgType === 'carPositions') {

        latestData = data;

        var name = data.data[0].id.name;
        var angle = data.data[0].angle;
        var pieceIndex = data.data[0].piecePosition.pieceIndex;
        var pieceDistance = data.data[0].piecePosition.inPieceDistance;        

        if (pieceDistance < 0) {
            pieceDistance *= -1.0;
        }

        var distanceBeforeNextPiece = trackPieces[pieceIndex].length - pieceDistance;
        var nextPieceMaxSpeed = 0;
        
        if ((pieceIndex + 1) === trackPieces.length)
        {
            nextPieceMaxSpeed = trackPieces[0].maxSpeed;
        }
        else
        {
            nextPieceMaxSpeed = trackPieces[pieceIndex + 1].maxSpeed;
        }

        if (angle < 0) {
            angle = angle * -1;
        }

        var gas = 0.6;

        /*
        if (angle > 1) {
            var lastFix = lastAngle.toFixed(2);
            var angleFix = angle.toFixed(2);
            var speedFix = lastSpeed.toFixed(2);
            var difference = (angle - lastAngle).toFixed(2);

            //console.log('Angle: ' + angle.toFixed(2) + ' (difference: ' + (angle - lastAngle).toFixed(2) + '), speed: ' + lastSpeed.toFixed(2));
            //console.log('Last angle: ' + lastFix + ', angle: ' + angleFix + ' (diff: ' + difference + '), speed: ' + speedFix);
        }
        */

        lastAngle = angle;

        if (pieceIndex !== lastPieceIndex) {
            lastPieceIndex = pieceIndex;
            lastDistance = pieceDistance;

            PrintPieceInfo(trackPieces[pieceIndex], pieceIndex);
            latestMaxSpeed = trackPieces[pieceIndex].maxSpeed;
        }
        else
        {
            lastSpeed = pieceDistance - lastDistance;
            lastDistance = pieceDistance;            
        }        

        if (lastSpeed > (nextPieceMaxSpeed * 10)) {
            var temp = lastSpeed / (nextPieceMaxSpeed * 10);

            var timeNeededToBreak;

            if (temp === 0) {
                timeNeededToBreak = 0;
            } else {
                timeNeededToBreak = Math.log(temp) / Math.log(0.98);
            }

            if (timeNeededToBreak < 0) {
                timeNeededToBreak *= -1;
            }

            var distanceNeeded = 0;

            var roundedTimeNeeded;
        
            if (timeNeededToBreak === 0) {
                roundedTimeNeeded = 0;
            } else {
                roundedTimeNeeded = Math.ceil(timeNeededToBreak) + 6;
            }
        
            //console.log('rounded time: ' + roundedTimeNeeded + ', time: ' + timeNeededToBreak);

            for (var u = 0; u < roundedTimeNeeded ; u++) {
                var pow = Math.pow(0.98, u);
                var tempSpeed = pow * lastSpeed;
                distanceNeeded += tempSpeed;

                //console.log('u: '+u+', pow: ' + pow.toFixed(2)+ ', lowered speed: '+tempSpeed.toFixed(2)+ ', last speed: '+lastSpeed.toFixed(2));

            }
            //console.log('distance needed: ' + distanceNeeded);
            console.log('time needed: ' + timeNeededToBreak.toFixed(2) + ', distance needed: ' + distanceNeeded.toFixed(2) + ', distance before next: ' + distanceBeforeNextPiece.toFixed(2));
            
            if (distanceBeforeNextPiece < distanceNeeded) {
                console.log('breaking too close! (speed: '+lastSpeed.toFixed(2)+')');
                //console.log('time needed: ' + timeNeededToBreak.toFixed(2) + ', distance needed: ' + distanceNeeded.toFixed(2) + ', distance before next: ' + distanceBeforeNextPiece.toFixed(2));
                //console.log('distance needed: ' + distanceNeeded.toFixed(2) + ', distance before next: ' + distanceBeforeNextPiece.toFixed(2));
                gas = 0;
            } else {
                //gas = latestMaxSpeed;
                gas = 1.0;
            }
        } else {
            gas = 1.0;
        }

        //console.log('piece index: ' + pieceIndex + ', pieceDistance: ' + pieceDistance + ', gas: ' + gas);
        //console.log('set gas to: ' + gas);
	    send({msgType: "throttle", data: gas});	
  }
  else {

    if (data.msgType === 'join') {
      //console.log('Joined')
    }
    else if (data.msgType === 'gameStart') {
      //console.log('Race started');
    }
    else if (data.msgType === 'gameEnd') {
      //console.log('Race ended');
    }
    else if (data.msgType === 'crash') {
        // check my car
        console.log('car crashed (index: ' + lastPieceIndex + ', speed: ' + lastSpeed+', angle: '+latestData.data[0].angle+')');
    }
    else if (data.msgType === 'lapFinished') {
        lap = 1;
        // check my car
        console.log('Lap time: ' + data.data.lapTime.millis);
    }
    else if (data.msgType === 'gameInit') {
        trackPieces = data.data.race.track.pieces;        

        for (var i = 0; i < trackPieces.length; i++) {
            trackPieces[i].maxSpeed = GetMaxSpeedForPiece(i);
            trackPieces[i].length = trackPieces[i].length || 6.28 * trackPieces[i].radius * trackPieces[i].angle / 360;

            if (trackPieces[i].length < 0) {
                trackPieces[i].length = trackPieces[i].length  * (-1.0);
            }

            console.log('Piece ' + i + ', Max speed:' + trackPieces[i].maxSpeed + ', Length: ' + trackPieces[i].length);
        }
    }

    send({msgType: "ping",data: {}});
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});


function GetMaxSpeedForPiece(index) {
    var totalLenght = 0;
    var tempPiece;
    var tempRadius;

    var radius = trackPieces[index].radius || -1;

    var minIndex = 0;

    for (var i = index; i >= 0; i--) {
        tempPiece = trackPieces[i];
        tempRadius = tempPiece.radius || 0;

        if (radius === tempRadius) {
            minIndex = i;
        } else {
            i = 0;
        }
    }

    var maxIndex = -1;

    for (var i = index; i < trackPieces.length; i++) {
        tempPiece = trackPieces[i];
        tempRadius = tempPiece.radius || 0;

        if (radius === tempRadius) {
            maxIndex = i;
        } else {
            i = trackPieces.length;
        }
    }

    for (var i = minIndex; i < maxIndex + 1; i++) {
        tempPiece = trackPieces[i];

        var tempLength = tempPiece.length || 6.28 * tempPiece.radius * tempPiece.angle / 360;

        if (tempLength < 0) {
            tempLength *= -1.0;
        }

        totalLenght += tempLength;
    }

    var maxSpeed = 0;

    for (var i = 1000; i > 600; i--) {
        var time = totalLenght / (i / 100);
        var a = 0.255 * (i / 100) - 1.6;
        //var a = 0.25 * (i / 100) - 1.6;
        var b = 0.13 * (i / 100) + 0.198;

        var angle = a * Math.pow(time, b);

        if (angle < 6) {
            maxSpeed = i / 1000;
            i = 50;
        }
    }
    return maxSpeed;
}

function PrintPieceInfo(piece, index) {
    console.log('--------------------------');

    var length = piece.length || 6.28 * piece.radius * piece.angle / 360;
    var radius = piece.radius || 0;
    var angle = piece.angle || 0;

    if (length < 0) {
        length *= -1;
    }

    if (angle === 0) {
        console.log('Piece [' + index + '], Length: ' + length + ' Speed: '+lastSpeed.toFixed(2)+' (maxSpeed: ' + piece.maxSpeed + ')');
    }
    else {
        console.log('Piece [' + index + '], Length: ' + length + ' Speed: ' + lastSpeed.toFixed(2) + ' (radius: ' + radius + ', angle: ' + angle + ', maxSpeed: ' + piece.maxSpeed + ')');
    }
}